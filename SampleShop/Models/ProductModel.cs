﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleShop.Models
{
    public class ProductModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public float prince { get; set; }
        public string description { get; set; }
        public int point { get; set; }
        public string image { get; set; }
        public int category { get; set; }
    }
}