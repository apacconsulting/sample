﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleShop.Models
{
    public class UserModel
    {
        public int userid { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public DateTime created_at { get; set; }
        public DateTime lastlogin { get; set; }
        public int role { get; set; }


    }
}