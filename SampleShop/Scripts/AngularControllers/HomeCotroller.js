﻿//Declaring angular module
var AnguarModule = angular.module('App', []);


//Declaring angular controller
// ApiCall---This is the service name which is in ApiCallService.js file
AnguarModule.controller('AppController', function ($scope, $http, ApiCall,Cart) {
    //Intital message value
    
    $scope.message = "";
    $scope.total = Cart.getTotal($scope);
    //Load dropdown in page load event  with get api call using service
    var result = ApiCall.GetApiCall("Products", "ProductHome").success(function (data) {
        data = $.parseJSON(JSON.parse(data));
        $scope.StateList = data;
    });

    $scope.btnbuy = function (id, name, prince, description, point, image, category,number) {
        
        Cart.addItem(id, name, prince, description, point, image, category, $scope, number);
        
        
    };

    

});


