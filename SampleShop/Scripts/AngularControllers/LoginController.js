﻿//Declaring angular module
var AnguarModule = angular.module('App', []);


//Declaring angular controller
// ApiCall---This is the service name which is in ApiCallService.js file
AnguarModule.controller('AppController', function ($scope, $http, ApiCall, $window) {
    //Intital message value
    $scope.message = "";

    //Load dropdown in page load event  with get api call using service
    /*var result = ApiCall.GetApiCall("product", "").success(function (data) {
        var data = $.parseJSON(JSON.parse(data));
        $scope.StateList = data;
    });*/
    //alert("sss")

    $scope.btnLogin = function () {
        var obj = {
            'username': $scope.username,
            'password': $scope.password
        };
        
        //Call Post method from web api in angular controller using angular service. I am passing string value to the web api through service.
        var result = ApiCall.PostApiCall("User", "loginAdmin", obj).success(function (data) {
            var data = $.parseJSON(JSON.parse(data));
            $scope.message = data;
            if (data == 1) {
                ApiCall.setCookie("Admin", "admin", 100);
                $window.location.href = '/Admin/Main';
            } else {
                alert("incorrect account");
            }
                
        });
        
        
    };


});