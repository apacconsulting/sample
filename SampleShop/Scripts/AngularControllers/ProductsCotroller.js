﻿//Declaring angular module
var AnguarModule = angular.module('App', []);


//Declaring angular controller
// ApiCall---This is the service name which is in ApiCallService.js file
AnguarModule.controller('AppController', function ($scope, $http, ApiCall, $document,Cart) {
    //Intital message value
    var id = angular.element($('#productid')).val();
    $scope.total = Cart.getTotal($scope);
    //console.log(name);

    //Load dropdown in page load event  with get api call using service
    ApiCall.GetApiCall("Category", "").success(function (data) {
        data = $.parseJSON(JSON.parse(data));
        $scope.StateList = data;
    });

    ApiCall.GetApiCall("Category", "").success(function (data) {
        data = $.parseJSON(JSON.parse(data));
        $scope.State = data;
    });

    ApiCall.GetApiCall("Products", "ProductList").success(function (data) {
        data = $.parseJSON(JSON.parse(data));
        $scope.productList = data;
    });

    $scope.show = function($id){
        ApiCall.GetApiCall("Products", "ProductList?id="+$id).success(function (data) {
            data = $.parseJSON(JSON.parse(data));
            $scope.productList = data;
        });
    }
    /*
    $scope.btnPostCall = function () {
        var obj = {
            'stateName': $scope.State.stateName
        };
        //Call Post method from web api in angular controller using angular service. I am passing string value to the web api through service.
        var result = ApiCall.PostApiCall("Values", "GetResult", obj).success(function (data) {
            var data = $.parseJSON(JSON.parse(data));
            $scope.message = data;
        });
    };*/

});


