﻿//Declaring angular module
var AnguarModule = angular.module('App', []);


//Declaring angular controller
// ApiCall---This is the service name which is in ApiCallService.js file
AnguarModule.controller('AppController', function ($scope, $http,Cart) {
    //Intital message value
    $scope.total = Cart.getTotal($scope);
    $scope.data = Cart.getProductList();

    $scope.remove = function (id) {
        Cart.removeItem(id, $scope);
        $scope.data = Cart.getProductList();
        $scope.total = Cart.getTotal($scope);
    }


});


