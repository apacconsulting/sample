﻿
AnguarModule.service('Cart', ['$http', function ($http) {
    var result;
    var total = 0;
    this.removeItem = function () {
    }

    this.editItem = function () {
    }

    function getCookie() {
        var key, val, res;
        //get all cookie
        var oldCookie = document.cookie.split(';');
        for (var i = 0; i < oldCookie.length; i++) {
            key = oldCookie[i].substr(0, oldCookie[i].indexOf("="));
            val = oldCookie[i].substr(oldCookie[i].indexOf("=") + 1);
            key = key.replace(/^\s+|\s+$/g, "");
            //find "user_cookie"
            if (key == "cart_product") {
                res = val;
            }
        }
        if (res == undefined) {
            return null;
        } else {
            res = JSON.parse(res);
            //res.sort(function (a, b) { return parseFloat(a.id) - parseFloat(b.id); });
            
            return res.cart_product;
        }
    }

    function sortByKey(array, key) {
        return array.sort(function (a, b) {
            var x = a[key]; var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    }

    this.addItem = function setCookie(id, name, prince, description, point, image, category, $scope,number) {
        var arr = new Array();
        var obj = new Object();
        total = 0;
        //add new cookie data
        obj.id = id;
        obj.name = name;
        obj.prince = prince;
        obj.description = description;
        obj.point = point;
        obj.image = image;
        obj.category = category;
        obj.number = number;
        var indexFlag = -1;
        var indexNumber = 1;
        var temp = getCookie();
        if (temp != null) {
            temp = sortByKey(temp, 'id');
            //concat new and old cookie data
            for (var i = 0; i < temp.length; i++) {
                var ob = new Object();
                ob.id = temp[i].id;
                ob.name = temp[i].name;
                ob.prince = temp[i].prince;
                ob.description = temp[i].description;
                ob.point = temp[i].point;
                ob.image = temp[i].image;
                ob.category = temp[i].category;
                ob.number = temp[i].number;
                //total += temp[i].prince;
                if (obj.id != temp[i].id) {
                    arr.push(ob);
                } else {
                    //ob.number = temp[i].number + number;
                    indexFlag = i;
                    indexNumber = temp[i].number;
                    
                }
                    
            }

        }
        arr.push(obj);
        
        if (arr != null)
           arr = sortByKey(arr, 'id');
        
        //get old cookie data

        //console.log(indexFlag);
        if (indexFlag != -1) {
            var a = arr[indexFlag].number + 1;
            arr[indexFlag].number = indexNumber +1;
            console.log(arr[indexFlag].number);
        }
        
        var objWarp = new Object();
        objWarp.cart_product = arr;
        var val = JSON.stringify(objWarp);

        //set cookie date expired
        var date = new Date();
        date.setTime(date.getTime() + (1 * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
        
        //create cookie
        document.cookie = "cart_product=" + val + expires;
        //console.log(document.cookie);
        this.getTotal($scope);

    }

    


    this.getProductList = function ($scope) {
        var arr = new Array();
        var temp = getCookie();
        if (temp != null) {
            //concat new and old cookie data
            for (var i = 0; i < temp.length; i++) {
                var ob = new Object();
                ob.id = temp[i].id;
                ob.name = temp[i].name;
                ob.prince = temp[i].prince;
                ob.description = temp[i].description;
                ob.point = temp[i].point;
                ob.image = temp[i].image;
                ob.category = temp[i].category;
                ob.number = temp[i].number;
                total += temp[i].prince;
                arr.push(ob);
            }
        }
        return arr;

    }

    this.removeItem = function (id, $scope) {
        var arr = new Array();
        var obj = new Object();
        var temp = getCookie();
        for (var i = 0; i < temp.length; i++) {
            console.log(temp[i].id + "-" + id);
            if (temp[i].id == id) {
                //temp.splice(i);
            } else {
                var ob = new Object();
                ob.id = temp[i].id;
                ob.name = temp[i].name;
                ob.prince = temp[i].prince;
                ob.description = temp[i].description;
                ob.point = temp[i].point;
                ob.image = temp[i].image;
                ob.category = temp[i].category;
                ob.number = temp[i].number;
                arr.push(ob);
                console.log(i);
            }
        }
        //arr.push(obj);
        document.cookie = "cart_product=" + ";expires=Thu, 01 Jan 1970 00:00:00 GMT";
        var objWarp = new Object();
        objWarp.cart_product = arr;
        var val = JSON.stringify(objWarp);
        console.log(val);
        //set cookie date expired
        var date = new Date();
        date.setTime(date.getTime() + (1 * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();

        //create cookie
        document.cookie = "cart_product=" + val + expires;
        //console.log(document.cookie);
        this.getTotal($scope);
        
    }


    this.getTotal = function ($scope) {
        var arr = new Array();
        total = 0;
        var temp = getCookie();
        if (temp != null) {
            //concat new and old cookie data
            for (var i = 0; i < temp.length; i++) {
                var ob = new Object();
                ob.id = temp[i].id;
                ob.name = temp[i].name;
                ob.prince = temp[i].prince;
                ob.description = temp[i].description;
                ob.point = temp[i].point;
                ob.image = temp[i].image;
                ob.category = temp[i].category;
                total += (temp[i].prince * temp[i].number);
                //arr.push(ob);
            }
            $scope.total = total;

        } else {
            $scope.total = 0;
        }
        return total;

    }

}]);

