﻿AnguarModule.service('ApiCall', ['$http', function ($http) {
    var result;

    // This is used for calling get methods from web api
    this.GetApiCall = function (controllerName, methodName) {
        result = $http.get('/api/' + controllerName + '/' + methodName).success(function (data, status) {
            result = (data);
        }).error(function () {
            //alert("Something went wrong get");
        });
        return result;
    };

    // This is used for calling post methods from web api with passing some data to the web api controller
    this.PostApiCall = function (controllerName, methodName, obj) {
        result = $http.post('/api/' + controllerName + '/' + methodName, obj).success(function (data, status) {
            result = (data);
        }).error(function () {
            //alert("Something went wrong post");
        });
        return result;
    };

    //set cookies
    this.setCookie = function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }



}]);

