﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using SampleShop.Models;

namespace SampleShop.Areas.Admin.Controllers
{
    public class MainController : Controller
    {

        // GET: Admin/Main
        public ActionResult Index()
        {
            if (Request.Cookies["Admin"] == null)
                return RedirectToAction("Login","/");
            return View();
        }

        // GET: Admin/Main/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/Main/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Main/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Main/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/Main/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Main/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Main/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
