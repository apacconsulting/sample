﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SampleDataAccess;
using SampleShop.Models;

namespace SampleShop.Areas.ApiSample.Controllers
{
    public class ProductsController : ApiController
    {
        [Route("api/Products/ProductList")]
        [HttpGet]
        public string ProductList()
        {
            using (sampleEntities entities = new sampleEntities())
            {
                string result = string.Empty;

                result = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(entities.products.ToList());
                      
                return result;
            }
        }

        [Route("api/Products/ProductHome")]
        [HttpGet]
        public string ProductHome()
        {
            using (sampleEntities entities = new sampleEntities())
            {
                string result = string.Empty;

                result = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(entities.products.Where(e => e.point == 1).ToList());

                return result;
            }
        }

        [Route("api/Products/ProductList")]
        [HttpGet]
        public string ProductList(int id)
        {
            using (sampleEntities entities = new sampleEntities())
            {
                string result = string.Empty;

                result = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(entities.products.Where(e => e.category == id).ToList());

                return result;
            }
        }
    }
        
}
