﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SampleDataAccess;
using SampleShop.Models;


namespace SampleShop.Areas.ApiSample.Controllers
{
    public class CategoryController : ApiController
    {
        // GET: api/Category
        public string Get()
        {
            using (sampleEntities entities = new sampleEntities())
            {
                string result = string.Empty;
                //return entities.products.Where(e => e.point == 1).ToList();
                result = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(entities.categories.ToList());
                return result;
            }
        }

        // GET: api/Category/5
        public string Product(int id)
        {
            using (sampleEntities entities = new sampleEntities())
            {
                string result = string.Empty;
                //return entities.products.Where(e => e.point == 1).ToList();
                result = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(entities.categories.Where(e => e.id == id).ToList());
                return result;
            }
        }

        [AcceptVerbs("GET")]
        public string Get(int id)
        {
            using (sampleEntities entities = new sampleEntities())
            {
                string result = string.Empty;
                //return entities.products.Where(e => e.point == 1).ToList();
                result = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(entities.categories.ToList());
                return result;
            }
        }

        // POST: api/Category
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Category/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Category/5
        public void Delete(int id)
        {
        }
    }
}
