﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SampleDataAccess;

namespace SampleShop.Areas.ApiSample.Controllers
{
    public class TestController : ApiController
    {
        [Route("api/Test/call")]
        [HttpGet]
        public string call()
        {
            using (sampleEntities entities = new sampleEntities())
            {
                string result = string.Empty;
                //return entities.products.Where(e => e.point == 1).ToList();
                result = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(entities.products.ToList());
                return result;
            }
        }

        [Route("api/Test/calls")]
        [HttpGet]
        public string calls()
        {
            using (sampleEntities entities = new sampleEntities())
            {
                string result = string.Empty;
                //return entities.products.Where(e => e.point == 1).ToList();
                result = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(entities.products.ToList());
                return "alo";
            }
        }
    }
}
