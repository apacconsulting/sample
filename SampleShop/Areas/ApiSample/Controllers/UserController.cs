﻿using Microsoft.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using SampleShop.Models;
using SampleDataAccess;
using System.Security.Cryptography;
using System.Text;

namespace SampleShop.Areas.ApiSample.Controllers
{
    public class UserController : ApiController
    {
        public static string PHPMd5Hash(string pass)
        {
            using (MD5 md5 = MD5.Create())
            {
                byte[] input = Encoding.UTF8.GetBytes(pass);
                byte[] hash = md5.ComputeHash(input);
                return BitConverter.ToString(hash).Replace("-", "");
            }
        }
        /*[HttpGet]
        public string role()
        {
            return "Gia Tri GET";
        }*/

        [HttpPost]
        public int loginAdmin(UserModel p)
        {
            using (sampleEntities entities = new sampleEntities())
            {
                int result = 0;
                //return entities.products.Where(e => e.point == 1).ToList();
                string encode = PHPMd5Hash(p.password);
                result = entities.accounts.Where(e => e.username == p.username && e.password == encode).ToList().Count;
                return result;
            }
        }

        
    }
}
