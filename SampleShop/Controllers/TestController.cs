﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleShop.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        [HttpPost]
        public ActionResult Index()
        {
            ViewBag.ReturnUrl = "POST";
            return View();
        }
    }
}