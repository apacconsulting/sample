﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SampleShop.Models;
using SampleDataAccess;

namespace SampleShop.Controllers
{
    public class ValuesController : ApiController
    {

        [HttpGet]
        public string GetDropdown()
        {
            using (sampleEntities entities = new sampleEntities())
            {
                string result = string.Empty;
                //return entities.products.Where(e => e.point == 1).ToList();
                result = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(entities.products.Where(e => e.point == 1).ToList());
                return result;
            }
        }
    }
}
