﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SampleShop.Startup))]
namespace SampleShop
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
